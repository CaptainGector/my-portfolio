# My Portfolio
Seriously, you should hire me.

## School Projects
- [A User-Interface for a Raspberry Pi and Touchscreen (So Much Linux!)](https://gitlab.com/CaptainGector/rpi_gui)
- [The primary communication code for that UI!](https://gitlab.com/CaptainGector/rpi_client)
- [The program that UI talked to - on another embdedded device, over the internet!](https://gitlab.com/CaptainGector/bbb_server)
- [Some project documents and papers from these classes (including the embdedded one!)](https://gitlab.com/CaptainGector/school-projects-documented)

## Check these other Repos Out - Programming & Hardware
- [A VFO - Powered by a SAMD21](https://gitlab.com/CaptainGector/samd21-vfo/-/tree/master)
- [This simple Wifi Remote Control for a Ham Radio](https://gitlab.com/CaptainGector/esp8266-ubitx-wifi)

## Other Projects
- [I did most of the leg-work on this, though not as much the writing of the blog post.](https://thalliatree.net/thc-plasma-cutter.html)

## Other Hardware
Sadly, I don't have most of my PCB work documented. I've helped design some pretty cool stuff, like 100W radio transmitters, RF energy harvesting (my capstone), voicemail for radio, custom keyboard work, PCB fabrication with 3d printers, and *muchhhhhh* more.
